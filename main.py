import h5py
import glob
import logging
import numpy as np
import sys

name2num = {
    'xpeak': 1,
    'zsigma': 2,
    'zemmitance': 3,
    'flux': 4,
    'xsigma': 5,
    'zpeak': 6,
    'xemmitance': 7
}


class HDDataset:
    def __init__(self, pinhole_dir, scalars_dir, log_level=logging.DEBUG):
        self.logger = logging.getLogger('tipper')
        self.logger.setLevel(log_level)
        self.logger.addHandler(logging.StreamHandler(sys.stdout))
        self.pinhole_dataset = None
        self.pinhole_dataset_meta = None
        self.scalars_dataset = None
        self.scalars_dataset_meta = None
        self.dataset = None
        self.labels = None
        self.pinhole_dir = pinhole_dir
        self.scalars_dir = scalars_dir

    def stack_values(self):
        self.stack_images()
        self.stack_scalars()

    def stack_images(self):
        files = glob.glob(self.pinhole_dir + "/*.h5")
        self.logger.debug(f"Image files: {files}")
        f = h5py.File(files[0], 'r')
        self.pinhole_dataset = f[('images')][()]
        self.pinhole_dataset_meta = f[('meta')][()]
        files.pop(0)
        for file in files:
            f = h5py.File(file, 'r')
            self.pinhole_dataset = np.concatenate((self.pinhole_dataset, f[('images')][()]))
            self.pinhole_dataset_meta = np.concatenate((self.pinhole_dataset_meta, f[('meta')][()]))
            self.logger.debug(f"Stack shapes: imgs {self.pinhole_dataset.shape}, "
                              f"imgs meta {self.pinhole_dataset_meta.shape}")

    def stack_scalars(self):
        files = glob.glob(self.scalars_dir + "/*.h5")
        self.logger.debug(f"Scalar files: {files}")
        f = h5py.File(files[0], 'r')
        self.scalars_dataset = f[('data')][()]
        self.scalars_dataset_meta = f[('meta')][()]
        files.pop(0)
        for file in files:
            f = h5py.File(file, 'r')
            self.scalars_dataset = np.concatenate((self.scalars_dataset, f[('data')][()]))
            self.logger.debug(f"Stack shapes: scalars {self.scalars_dataset.shape}")

    def correlate(self):
        """
        Method to be defined. Dummy implementation.
        :return:
        """
        scalar_timestamps = self.scalars_dataset[:, 0]
        first = True
        self.logger.debug(f"Correlating {self.pinhole_dataset_meta.shape} values")
        for id, el in enumerate(self.pinhole_dataset_meta):
            timestamp = el[0]
            ind = np.argmin(abs(scalar_timestamps - timestamp))
            if first:
                self.dataset = np.insert(self.scalars_dataset[ind, 1:], 0, timestamp)
                first = False
            else:
                row = np.insert(self.scalars_dataset[ind, 1:], 0, timestamp)
                self.dataset = np.vstack((self.dataset, row))
            self.logger.debug(f"Correlated {id} value")

    def evaluate(self):
        tmp1 = self.dataset[:, name2num['xemmitance']] < 11
        tmp2 = self.dataset[:, name2num['zemmitance']] < 0.8
        self.labels = np.logical_and(tmp1, tmp2)
        print(f"Sum of TRUE values: {np.sum(self.labels)}")

    def save(self, name):
        hf = h5py.File(f'D:\Baza danych\{name}.h5', 'w')
        hf.create_dataset('scalar_data', data=self.dataset)
        hf.create_dataset('image_data', data=self.pinhole_dataset, compression="gzip")
        hf.create_dataset('labels', data=self.labels)
        hf.close()
        print(f"Saved!!!")

def main():
    inst = HDDataset('D:\Baza danych\pinhole\P5', 'D:\Baza danych\scalars\P5', log_level=logging.DEBUG)
    inst.stack_values()
    inst.correlate()
    # choose from 'xpeak'  'zsigma' 'zemmitance' 'flux' 'xsigma' 'zpeak' 'xemmitance'
    inst.evaluate()
    inst.save("dataset_part5")
    print("Done")


if __name__ == '__main__':
    main()
