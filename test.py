import h5py
import tkinter as tk
from tkinter import simpledialog
from PIL import Image
import datetime

num2name = {
    1: 'xpeak',
    2: 'zsigma',
    3: 'zemmitance',
    4: 'flux',
    5: 'xsigma',
    6: 'zpeak',
    7: 'xemmitance'
}

ROOT = tk.Tk()
ROOT.withdraw()

hf = h5py.File('D:\Baza danych\dataset_part1.h5', 'r')

def prepare():
    n1 = hf.get('scalar_data')
    n2 = hf.get('image_data')
    n3 = hf.get('labels')
    return n1, n2, n3


def main():
    data = prepare()
    while True:
        try:
            USER_INP = simpledialog.askstring(title="Test dataset", prompt="Które pokazać?")
            scalar = data[0][int(USER_INP)]
            img = data[1][int(USER_INP)]
            label = data[2][int(USER_INP)]
            imag = Image.fromarray(img)
            imag.show()
            for i, s in enumerate(scalar):
                if i == 0:
                    print(f"Timestamp: {datetime.datetime.fromtimestamp(s).strftime('%c')}")
                else:
                    print(f"{num2name[i]}, {s}")
            print(f"Flag: {label}")
        except Exception as e:
            print("Bye!")
            break


if __name__ == '__main__':
    main()
